# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext as _
from apps.pythia.models import CourseProperties
from apps.merovingian.functions import default_sgroup_name


# --- Search ---

class SearchForm(forms.Form):
    name = forms.CharField(label = '',
                           required = False)
    
class CourseSelectForm(forms.Form):
    course   = forms.ChoiceField(label = _(u'Course'),
                              required = True)
    
    def set_courses(self, courses):
        self.fields['course'].choices = [ (m.id, unicode(m)) for m in courses ]

class CourseStudentsNumberForm(forms.Form):
    course   = forms.IntegerField(label = '',
                              required = True,
                              widget = forms.HiddenInput())

    instance = None

    def set_course(self, course):
        
        self.instance = course
        
        self.fields['course'].initial = course.id
        self.fields['course'].label = course
        
        if course.start_date:
            
            counts_map = {}    
            for p in course.courseproperties_set.all():
                counts_map[p.semester] = p.token_number
        
            for i in self.instance.get_current_semesters():
                field = forms.IntegerField(required = True, min_value=0)
                field.initial = counts_map[i] if i in counts_map else 0
                
                self.fields[str(i)] = field
        
    def save(self):
        if self.instance:
            properties_map = {}    
            for p in self.instance.courseproperties_set.all():
                properties_map[p.semester] = p
                
            for sem in self.instance.get_current_semesters():
                value = int(self.cleaned_data[str(sem)])
                if not sem in properties_map:
                    p = CourseProperties()
                    p.course = self.instance
                    p.semester = sem
                    p.token_number = value
                    p.save()
                else:
                    p = properties_map[sem]
                    if p.token_number != value:
                        p.token_number = value
                        p.save()
            
            
def course_students_number_formset_factory(*args, **kwargs):
    courses = kwargs.pop('courses', [])
    courses_formset = forms.formsets.formset_factory(CourseStudentsNumberForm, 
                                                      extra = courses.count(), 
                                                      max_num = courses.count(),
                                                      formset = RequiredFormSet)(*args, **kwargs)
    for i in range(courses.count()):
        courses_formset.forms[i].set_course(courses[i])
    return courses_formset
       
        
                    
class TokenLoginForm(forms.Form):
    token = forms.CharField(label = _(u'Token'),
                            max_length = 10)
    email = forms.EmailField(label=_(u'E-mail'))

class SubjectsForm(forms.Form):
    subjects = forms.MultipleChoiceField(label = _(u'Subjects'),
                                         required = True,
                                         choices = [],
                                         widget = forms.CheckboxSelectMultiple())
    
    def set_subjects(self, subjects):
        choices = []
        for s in subjects:
            name = unicode(s)
            sname = unicode(s.module.get_sgroup().name)
            default_sname = default_sgroup_name()
            if sname != default_sname:
                name += " " + _(u"(specialty: %s)") % sname
            choices.append((s.id, name))
        self.fields['subjects'].choices = choices 

class TeacherForm(forms.Form):
    teacher = forms.ChoiceField(label = _(u'Teacher'),
                                required = True,
                                choices = [],
                                widget = forms.Select())
    selected_teacher = forms.IntegerField(label = '',
                                 required = False,
                                 widget = forms.HiddenInput())
    
    subject = forms.IntegerField(label = '',
                                 required = True,
                                 widget = forms.HiddenInput())

    def set_teachers(self, teachers):
        self.fields['teacher'].choices = [('', '---')] + [(t.id, unicode(t)) for t in teachers]
        
    def add_teacher(self, teacher):
        for ch in self.fields['teacher'].choices:
            if teacher.id == ch[0]:
                return
        self.fields['teacher'].choices.append((teacher.id, unicode(teacher)))

    def set_subject(self, subject):
        self.fields['subject'].initial = subject

class AbstractQuestionForm(forms.Form):
    question = forms.IntegerField(label = '',
                                  required = True,
                                  widget = forms.HiddenInput())

    def set_question(self, question):
        self.fields['question'].initial = question

class QuestionAnswerForm(AbstractQuestionForm):
    answer = forms.ChoiceField(label = '',
                               required = True,
                               choices = [('', '---')],
                               widget = forms.Select())

    def set_label(self, label):
        self.fields['answer'].label = label

    def set_choices(self, choices):
        self.fields['answer'].choices = [('', '---')] + [(a.id, unicode(a)) for a in choices]

class QuestionTextForm(AbstractQuestionForm):
    answer = forms.CharField(label = '', 
                             max_length = 2048, 
                             required = False, 
                             widget = forms.Textarea(),
                             help_text = _(u'Answer must have at most 2048 characters'))

    def set_label(self, label):
        self.fields['answer'].label = label

class RequiredFormSet(forms.formsets.BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

def question_answer_formset_factory(*args, **kwargs):
    from apps.pythia.models import Answer
    questions = kwargs.pop('questions', [])
    question_formset = forms.formsets.formset_factory(QuestionAnswerForm, 
                                                      extra = questions.count(), 
                                                      max_num = questions.count(),
                                                      formset = RequiredFormSet)(*args, **kwargs)
    for i in range(questions.count()):
        question_formset.forms[i].set_question(questions[i].id)
        question_formset.forms[i].set_label(questions[i].name)
        question_formset.forms[i].set_choices(Answer.objects.filter(question__exact = questions[i]))
        question_formset.forms[i].question_id = questions[i].id
    return question_formset

def question_text_formset_factory(*args, **kwargs):
    questions = kwargs.pop('questions', [])
    question_formset = forms.formsets.formset_factory(QuestionTextForm, 
                                                      extra = questions.count(), 
                                                      max_num = questions.count())(*args, **kwargs)
    for i in range(questions.count()):
        question_formset.forms[i].set_question(questions[i].id)
        question_formset.forms[i].set_label(questions[i].name)
        question_formset.forms[i].question_id = questions[i].id
    return question_formset
