# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MinLengthValidator
from django.utils.translation import ugettext_lazy as _
from apps.megacity.models import Student

from apps.syjon.lib.validators import validate_white_space
from apps.trainman.models import Teacher
from apps.merovingian.models import Subject, Course



class AbstractUniqueName(models.Model):
    class Meta:
        abstract = True
        ordering = ('name',)
        verbose_name = _(u'Name')
        verbose_name_plural = _(u'Names')

    name = models.CharField(max_length = 256,
                            unique = True,
                            verbose_name = _(u'name'),
                            validators = [MinLengthValidator(2), validate_white_space])

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.name = self.name.strip()
        super(AbstractUniqueName, self).save(*args, **kwargs)

class AbstractName(models.Model):
    class Meta:
        abstract = True
        ordering = ('name',)
        verbose_name = _(u'Name')
        verbose_name_plural = _(u'Names')

    name = models.CharField(max_length = 256,
                            verbose_name = _(u'name'),
                            validators = [MinLengthValidator(2), validate_white_space])

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.name = self.name.strip()
        super(AbstractName, self).save(*args, **kwargs)

class AbstractActive(AbstractUniqueName):
    class Meta:
        abstract = True
        ordering = ('-is_active', 'name')
        verbose_name = _(u'Name, active')
        verbose_name_plural = _(u'Names, active')
        
    is_active = models.BooleanField(db_column = 'is_active',
                                    default = True,
                                    verbose_name = _(u'Active'))
    
    def __unicode__(self):
        return '%s, %s' % (unicode(self.name), unicode(self.is_active))

    def set_active(self, active):
        self.is_active = active
        self.save()

class Poll(AbstractActive):
    class Meta:
        db_table = 'pythia_poll'
        verbose_name = _(u'Poll')
        verbose_name_plural = _(u'Polls')

    start_date = models.DateField(db_column = 'start_date',
                                  verbose_name = _(u'Start date'))
    end_date = models.DateField(db_column = 'end_date',
                                verbose_name = _(u'End date'))

    def __unicode__(self):
        s = unicode(self.name)
        if self.is_active:
            s += ' (A)'
        s += ' (%s - %s)' % (unicode(self.start_date), unicode(self.end_date))
        return s

    def save(self, *args, **kwargs):
        if self.is_active:
            for d in Poll.objects.filter(is_active = True):
                d.set_active(False)
        super(Poll, self).save(*args, **kwargs)

class CourseProperties(models.Model):
    class Meta:
        db_table = 'pythia_course_properties'
        verbose_name = _(u'Course properties')
        verbose_name_plural = _(u'Course properties')

    course = models.ForeignKey('merovingian.Course',
                              db_column = 'id_course',
                              verbose_name = _(u'Course'))
    semester = models.IntegerField(db_column = 'semester',
                                   max_length = 2,
                                   verbose_name = _(u'Semester'))
    token_number = models.IntegerField(db_column = 'token_number',
                                       verbose_name = _(u'Number of tokens'))

    def __unicode__(self):
        return '%s - %s : %s' % (unicode(self.course), self.semester, self.token_number)

class Token(AbstractActive):
    class Meta:
        db_table = 'pythia_token'
        verbose_name = _(u'Token')
        verbose_name_plural = _(u'Tokens')

    date = models.DateTimeField(db_column = 'date', null=True, blank=True,
                                verbose_name = _(u'Fill in date'))
    semester = models.IntegerField(db_column = 'semester',
                                   max_length = 2,
                                   verbose_name = _(u'Semester'))
    course = models.ForeignKey('merovingian.Course',
                              db_column = 'id_course',
                              verbose_name = _(u'Course'))
    poll = models.ForeignKey('Poll',
                             db_column = 'id_poll',
                             verbose_name = _(u'Poll'))
    student = models.ForeignKey(Student, verbose_name=_(u'Student'),
                                blank=True, null=True)
    sent = models.BooleanField(verbose_name=_(u'Sent'), default=False)

    def __unicode__(self):
        return '%s, %s, %s' % (self.name, self.course, self.semester)

QUESTION_TYPE_GROUP = 1
QUESTION_TYPE_GROUP_DESCRIPTION = 2
QUESTION_TYPE_CLOSEDEND_EVALUATION = 3
QUESTION_TYPE_CLOSEDEND_FINAL = 4
QUESTION_TYPE_OPEN_EVALUATION = 5
QUESTION_TYPE_OPEN_FINAL = 6

RESULT_ALLOWED_LIST = [1,2,3,4,5,6,7]

class QuestionType(AbstractName):
    class Meta:
        db_table = 'pythia_question_type'
        verbose_name = _(u'Question type')
        verbose_name_plural = _(u'Question types')

class Question(AbstractName):
    class Meta:
        db_table = 'pythia_question'
        ordering = ('sequence',)
        verbose_name = _(u'Question')
        verbose_name_plural = _(u'Questions')

    sequence = models.IntegerField(db_column = 'sequence',
                                   verbose_name = _(u'Sequence'))
    type = models.ForeignKey('QuestionType',
                             db_column = 'id_type',
                             verbose_name = _(u'Type'))
    poll = models.ForeignKey('Poll',
                             db_column = 'id_poll',
                             verbose_name = _(u'Poll'))
    weight = models.FloatField(db_column = 'weight',
                                 verbose_name = _(u'Weight'))

    def __unicode__(self):
        return unicode(self.name)

class AbstractTokenAnswer(models.Model):
    class Meta:
        abstract = True
        ordering = ('token',)
        verbose_name = _(u'Answer')
        verbose_name_plural = _(u'Answers')

    token = models.ForeignKey('Token',
                              db_column = 'id_token',
                              verbose_name = _(u'Token'))
    question = models.ForeignKey('Question',
                                 db_column = 'id_question',
                                 verbose_name = _(u'Question'))
    subject = models.ForeignKey('merovingian.Subject',
                                db_column = 'id_subject',
                                null = True,
                                blank = True,
                                verbose_name = _(u'Subject'))
    teacher = models.ForeignKey('trainman.Teacher',
                                db_column = 'id_teacher',
                                null = True,
                                blank = True,
                                verbose_name = _(u'Teacher'))

    def __unicode__(self):
        return '%s - %s - %s - %s' % (self.token.name, self.subject, self.teacher, self.question)

class Answer(AbstractName):
    class Meta:
        db_table = 'pythia_answer'
        ordering = ('sequence',)
        verbose_name = _(u'Answer')
        verbose_name_plural = _(u'Answers')

    sequence = models.IntegerField(db_column = 'sequence',
                                   verbose_name = _(u'Sequence'))
    weight = models.FloatField(db_column = 'weight',
                                 verbose_name = _(u'Weight'))
    question = models.ForeignKey('Question',
                                 db_column = 'id_question',
                                 verbose_name = _(u'Question'))
    mark = models.FloatField(db_column = 'mark',
                             verbose_name = _(u'Mark'))

class TokenAnswer(AbstractTokenAnswer):
    class Meta:
        db_table = 'pythia_token_answer'
        verbose_name = _(u'Closed-end answer')
        verbose_name_plural = _(u'Closed-end answers')

    answer = models.ForeignKey('Answer',
                               db_column = 'id_answer',
                               verbose_name = _(u'Answer'))

    def __unicode__(self):
        return super(TokenAnswer, self).__unicode__() + ' - ' + unicode(self.answer)

class TokenText(AbstractTokenAnswer):
    class Meta:
        db_table = 'pythia_token_text'
        verbose_name = _(u'Open answer')
        verbose_name_plural = _(u'Open answers')

    text = models.CharField(db_column = 'text',
                            max_length = 2048,
                            verbose_name = _(u'Answers'))

    def __unicode__(self):
        return super(TokenText, self).__unicode__() + ' - ' + unicode(self.text)
    
class ResultTeacher(models.Model):
    """
    Summary result for teacher
    """
    class Meta:
        db_table ='pythia_result_teacher'
        verbose_name = _(u'Poll result')
        verbose_name_plural = _(u'Poll results')
        
    teacher=models.ForeignKey(Teacher,db_column = 'id_teacher',verbose_name = _(u'Teacher'))
    votes  =models.IntegerField(db_column = 'votes', verbose_name = _(u'Votes'))
    mark   =models.FloatField(db_column = 'mark' , verbose_name = _(u'Mark' ))
    poll   =models.ForeignKey('Poll',db_column = 'id_poll',verbose_name = _(u'Poll'))
    
class ResultTeacherSubjectCourse(models.Model):
    """
    Summary result for one subject conducted by particular teacher 
    """
    class Meta:
        db_table ='pythia_result_teacher_subject_course'
        verbose_name = _(u'Poll result')
        verbose_name_plural = _(u'Poll results')
        
    teacher = models.ForeignKey(Teacher,db_column = 'id_teacher',verbose_name = _(u'Teacher'))
    subject = models.ForeignKey(Subject,db_column = 'id_subject',verbose_name = _(u'Subject'))
    course  = models.ForeignKey(Course,db_column = 'id_course',verbose_name = _(u'Course'))
    votes   = models.IntegerField(db_column = 'votes', verbose_name = _(u'Votes'))
    mark    = models.FloatField(db_column = 'mark' , verbose_name = _(u'Mark' ))
    poll    = models.ForeignKey('Poll',db_column = 'id_poll',verbose_name = _(u'Poll'))
    
class ResultCourse(models.Model):
    """
    Summary result for course.
    """
    class Meta:
        db_table ='pythia_result_course'
        verbose_name = _(u'Poll result')
        verbose_name_plural = _(u'Poll results')
        
    course  =models.ForeignKey(Course,db_column = 'id_course',verbose_name = _(u'Course'))    
    votes  =models.IntegerField(db_column = 'votes', verbose_name = _(u'Votes'))
    mark   =models.FloatField(db_column = 'mark' , verbose_name = _(u'Mark' ))
    poll   =models.ForeignKey('Poll',db_column = 'id_poll',verbose_name = _(u'Poll'))
 


