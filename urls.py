# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns

urlpatterns = patterns(
    'apps.pythia.views',
    (r'^login/$', 'login'),
    (r'^login/email/(?P<email>[^\/]+)/token/(?P<token>\w+)/$', 'login_link'),
    (r'^first/', 'first'),
    (r'^find_teacher/', 'find_teacher'),
    (r'^wizard/', 'wizard'),
    (r'^last/', 'last'),
    (r'^logout/', 'logout'),
    
    #(r'^count/(?P<letter>\w)/$', 'count'),
    (r'^stats/$', 'stats'),
    (r'^stats_question/(?P<question_id>\d+)/$', 'stats_question'),
    (r'^results/$', 'result_polls'),
    (r'^results/(?P<poll_id>\d+)/$', 'result_poll_teacherlist'),
    (r'^results/(?P<poll_id>\d+)/(?P<teacher_id>\d+)$', 'result_poll'),
    (r'^results_print/(?P<poll_id>\d+)/(?P<teacher_id>\d+)$', 'result_poll_print'),
    (r'^results/(?P<poll_id>\d+)/(?P<teacher_id>\d+)/(?P<subject_id>\d+)$', 'result_poll'),
    (r'^results/(?P<poll_id>\d+)/$', 'result_poll_teacherlist'),
    (r'^groupresults/(?P<poll_id>\d+)/(?P<department_id>\d+)$', 'result_poll_department'),
    (r'^groupresults/(?P<poll_id>\d+)/(?P<department_id>\d+)/(?P<mode>\w+)$', 'result_poll_department'),
    #(r'^$', 'index'),
)
