function id_to_windowname(text) {
    text = text.replace(/\./g, '__dot__');
    text = text.replace(/\-/g, '__dash__');
    return text;
}

function windowname_to_id(text) {
    text = text.replace(/__dot__/g, '.');
    text = text.replace(/__dash__/g, '-');
    return text;
}

function show_window(href, name)
{
    var win = window.open(href, id_to_windowname(name), 'height=500,width=800,resizable=yes,scrollbars=yes');
    win.focus();
}

function close_window(win, id, name)
{
	var opt = document.createElement('option');
	opt.innerHTML = name;
	opt.value = id;
	
    document.getElementById('id_selected_teacher').value = id;
    document.getElementById('id_teacher').appendChild(opt);
    document.getElementById('id_teacher').value = id;
    win.close();
}

