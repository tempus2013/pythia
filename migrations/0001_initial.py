# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Poll'
        db.create_table('pythia_poll', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True, db_column='is_active')),
            ('start_date', self.gf('django.db.models.fields.DateField')(db_column='start_date')),
            ('end_date', self.gf('django.db.models.fields.DateField')(db_column='end_date')),
        ))
        db.send_create_signal('pythia', ['Poll'])

        # Adding model 'CourseProperties'
        db.create_table('pythia_course_properties', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Course'], db_column='id_course')),
            ('semester', self.gf('django.db.models.fields.IntegerField')(max_length=2, db_column='semester')),
            ('token_number', self.gf('django.db.models.fields.IntegerField')(db_column='token_number')),
        ))
        db.send_create_signal('pythia', ['CourseProperties'])

        # Adding model 'Token'
        db.create_table('pythia_token', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True, db_column='is_active')),
            ('date', self.gf('django.db.models.fields.DateTimeField')(db_column='date')),
            ('semester', self.gf('django.db.models.fields.IntegerField')(max_length=2, db_column='semester')),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Course'], db_column='id_course')),
            ('poll', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Poll'], db_column='id_poll')),
        ))
        db.send_create_signal('pythia', ['Token'])

        # Adding model 'QuestionType'
        db.create_table('pythia_question_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal('pythia', ['QuestionType'])

        # Adding model 'Question'
        db.create_table('pythia_question', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('sequence', self.gf('django.db.models.fields.IntegerField')(db_column='sequence')),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.QuestionType'], db_column='id_type')),
            ('poll', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Poll'], db_column='id_poll')),
            ('weight', self.gf('django.db.models.fields.FloatField')(db_column='weight')),
        ))
        db.send_create_signal('pythia', ['Question'])

        # Adding model 'Answer'
        db.create_table('pythia_answer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('sequence', self.gf('django.db.models.fields.IntegerField')(db_column='sequence')),
            ('weight', self.gf('django.db.models.fields.FloatField')(db_column='weight')),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Question'], db_column='id_question')),
            ('mark', self.gf('django.db.models.fields.FloatField')(db_column='mark')),
        ))
        db.send_create_signal('pythia', ['Answer'])

        # Adding model 'TokenAnswer'
        db.create_table('pythia_token_answer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('token', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Token'], db_column='id_token')),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Question'], db_column='id_question')),
            ('subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Subject'], null=True, db_column='id_subject', blank=True)),
            ('teacher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Teacher'], null=True, db_column='id_teacher', blank=True)),
            ('answer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Answer'], db_column='id_answer')),
        ))
        db.send_create_signal('pythia', ['TokenAnswer'])

        # Adding model 'TokenText'
        db.create_table('pythia_token_text', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('token', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Token'], db_column='id_token')),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Question'], db_column='id_question')),
            ('subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Subject'], null=True, db_column='id_subject', blank=True)),
            ('teacher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Teacher'], null=True, db_column='id_teacher', blank=True)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=2048, db_column='text')),
        ))
        db.send_create_signal('pythia', ['TokenText'])

        # Adding model 'ResultTeacher'
        db.create_table('pythia_result_teacher', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('teacher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Teacher'], db_column='id_teacher')),
            ('votes', self.gf('django.db.models.fields.IntegerField')(db_column='votes')),
            ('mark', self.gf('django.db.models.fields.FloatField')(db_column='mark')),
            ('poll', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Poll'], db_column='id_poll')),
        ))
        db.send_create_signal('pythia', ['ResultTeacher'])

        # Adding model 'ResultTeacherSubjectCourse'
        db.create_table('pythia_result_teacher_subject_course', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('teacher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Teacher'], db_column='id_teacher')),
            ('subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Subject'], db_column='id_subject')),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Course'], db_column='id_course')),
            ('votes', self.gf('django.db.models.fields.IntegerField')(db_column='votes')),
            ('mark', self.gf('django.db.models.fields.FloatField')(db_column='mark')),
            ('poll', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Poll'], db_column='id_poll')),
        ))
        db.send_create_signal('pythia', ['ResultTeacherSubjectCourse'])

        # Adding model 'ResultCourse'
        db.create_table('pythia_result_course', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Course'], db_column='id_course')),
            ('votes', self.gf('django.db.models.fields.IntegerField')(db_column='votes')),
            ('mark', self.gf('django.db.models.fields.FloatField')(db_column='mark')),
            ('poll', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pythia.Poll'], db_column='id_poll')),
        ))
        db.send_create_signal('pythia', ['ResultCourse'])


    def backwards(self, orm):
        # Deleting model 'Poll'
        db.delete_table('pythia_poll')

        # Deleting model 'CourseProperties'
        db.delete_table('pythia_course_properties')

        # Deleting model 'Token'
        db.delete_table('pythia_token')

        # Deleting model 'QuestionType'
        db.delete_table('pythia_question_type')

        # Deleting model 'Question'
        db.delete_table('pythia_question')

        # Deleting model 'Answer'
        db.delete_table('pythia_answer')

        # Deleting model 'TokenAnswer'
        db.delete_table('pythia_token_answer')

        # Deleting model 'TokenText'
        db.delete_table('pythia_token_text')

        # Deleting model 'ResultTeacher'
        db.delete_table('pythia_result_teacher')

        # Deleting model 'ResultTeacherSubjectCourse'
        db.delete_table('pythia_result_teacher_subject_course')

        # Deleting model 'ResultCourse'
        db.delete_table('pythia_result_course')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'merovingian.course': {
            'Meta': {'ordering': "('-is_active', 'name', '-level__name', '-type__name', 'profile__name', 'start_date')", 'object_name': 'Course', 'db_table': "'merv_course'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'end_date'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_first': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'db_column': "'is_first'", 'blank': 'True'}),
            'is_last': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'db_column': "'is_last'", 'blank': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseLevel']", 'db_column': "'id_level'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseProfile']", 'null': 'True', 'db_column': "'id_profile'", 'blank': 'True'}),
            'semesters': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'db_column': "'semesters'", 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'start_date'", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseType']", 'db_column': "'id_type'"}),
            'years': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'db_column': "'years'", 'blank': 'True'})
        },
        'merovingian.courselevel': {
            'Meta': {'object_name': 'CourseLevel', 'db_table': "'merv_course_level'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.courseprofile': {
            'Meta': {'object_name': 'CourseProfile', 'db_table': "'merv_course_profile'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.coursetype': {
            'Meta': {'object_name': 'CourseType', 'db_table': "'merv_course_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.didacticoffer': {
            'Meta': {'object_name': 'DidacticOffer', 'db_table': "'merv_didactic_offer'"},
            'end_date': ('django.db.models.fields.DateField', [], {'db_column': "'end_date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'db_column': "'start_date'"})
        },
        'merovingian.module': {
            'Meta': {'ordering': "['name']", 'object_name': 'Module', 'db_table': "'merv_module'"},
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.ModuleType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'merovingian.moduletype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'ModuleType', 'db_table': "'merv_module_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.subject': {
            'Meta': {'ordering': "('semester', 'name', '-type', 'assessment')", 'object_name': 'Subject', 'db_table': "'merv_subject'"},
            'assessment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectAssessment']", 'db_column': "'id_assessment'"}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'hours': ('django.db.models.fields.FloatField', [], {'db_column': "'hours'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']", 'db_column': "'id_module'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'semester': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'semester'"}),
            'teachers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['trainman.Teacher']", 'null': 'True', 'through': "orm['merovingian.SubjectToTeacher']", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectType']", 'db_column': "'id_type'"})
        },
        'merovingian.subjectassessment': {
            'Meta': {'ordering': "('name',)", 'object_name': 'SubjectAssessment', 'db_table': "'merv_subject_assessment'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.subjecttoteacher': {
            'Meta': {'object_name': 'SubjectToTeacher', 'db_table': "'merv_subject__to__teacher'"},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'db_column': "'description'", 'blank': 'True'}),
            'groups': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'groups'"}),
            'hours': ('django.db.models.fields.FloatField', [], {'max_length': '3', 'db_column': "'hours'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']", 'db_column': "'id_subject'"}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']", 'db_column': "'id_teacher'"})
        },
        'merovingian.subjecttype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'SubjectType', 'db_table': "'merv_subject_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'pythia.answer': {
            'Meta': {'ordering': "('sequence',)", 'object_name': 'Answer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mark': ('django.db.models.fields.FloatField', [], {'db_column': "'mark'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Question']", 'db_column': "'id_question'"}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'db_column': "'sequence'"}),
            'weight': ('django.db.models.fields.FloatField', [], {'db_column': "'weight'"})
        },
        'pythia.courseproperties': {
            'Meta': {'object_name': 'CourseProperties', 'db_table': "'pythia_course_properties'"},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Course']", 'db_column': "'id_course'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'semester': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'semester'"}),
            'token_number': ('django.db.models.fields.IntegerField', [], {'db_column': "'token_number'"})
        },
        'pythia.poll': {
            'Meta': {'object_name': 'Poll'},
            'end_date': ('django.db.models.fields.DateField', [], {'db_column': "'end_date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_column': "'is_active'"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'db_column': "'start_date'"})
        },
        'pythia.question': {
            'Meta': {'ordering': "('sequence',)", 'object_name': 'Question'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'poll': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Poll']", 'db_column': "'id_poll'"}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'db_column': "'sequence'"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.QuestionType']", 'db_column': "'id_type'"}),
            'weight': ('django.db.models.fields.FloatField', [], {'db_column': "'weight'"})
        },
        'pythia.questiontype': {
            'Meta': {'object_name': 'QuestionType', 'db_table': "'pythia_question_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'pythia.resultcourse': {
            'Meta': {'object_name': 'ResultCourse', 'db_table': "'pythia_result_course'"},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Course']", 'db_column': "'id_course'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mark': ('django.db.models.fields.FloatField', [], {'db_column': "'mark'"}),
            'poll': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Poll']", 'db_column': "'id_poll'"}),
            'votes': ('django.db.models.fields.IntegerField', [], {'db_column': "'votes'"})
        },
        'pythia.resultteacher': {
            'Meta': {'object_name': 'ResultTeacher', 'db_table': "'pythia_result_teacher'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mark': ('django.db.models.fields.FloatField', [], {'db_column': "'mark'"}),
            'poll': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Poll']", 'db_column': "'id_poll'"}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']", 'db_column': "'id_teacher'"}),
            'votes': ('django.db.models.fields.IntegerField', [], {'db_column': "'votes'"})
        },
        'pythia.resultteachersubjectcourse': {
            'Meta': {'object_name': 'ResultTeacherSubjectCourse', 'db_table': "'pythia_result_teacher_subject_course'"},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Course']", 'db_column': "'id_course'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mark': ('django.db.models.fields.FloatField', [], {'db_column': "'mark'"}),
            'poll': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Poll']", 'db_column': "'id_poll'"}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']", 'db_column': "'id_subject'"}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']", 'db_column': "'id_teacher'"}),
            'votes': ('django.db.models.fields.IntegerField', [], {'db_column': "'votes'"})
        },
        'pythia.token': {
            'Meta': {'object_name': 'Token'},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Course']", 'db_column': "'id_course'"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'db_column': "'date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_column': "'is_active'"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'poll': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Poll']", 'db_column': "'id_poll'"}),
            'semester': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'semester'"})
        },
        'pythia.tokenanswer': {
            'Meta': {'object_name': 'TokenAnswer', 'db_table': "'pythia_token_answer'"},
            'answer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Answer']", 'db_column': "'id_answer'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Question']", 'db_column': "'id_question'"}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']", 'null': 'True', 'db_column': "'id_subject'", 'blank': 'True'}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']", 'null': 'True', 'db_column': "'id_teacher'", 'blank': 'True'}),
            'token': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Token']", 'db_column': "'id_token'"})
        },
        'pythia.tokentext': {
            'Meta': {'object_name': 'TokenText', 'db_table': "'pythia_token_text'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Question']", 'db_column': "'id_question'"}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']", 'null': 'True', 'db_column': "'id_subject'", 'blank': 'True'}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']", 'null': 'True', 'db_column': "'id_teacher'", 'blank': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'db_column': "'text'"}),
            'token': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['pythia.Token']", 'db_column': "'id_token'"})
        },
        'trainman.department': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Department'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_department'", 'to': "orm['trainman.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.DepartmentType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'trainman.departmenttype': {
            'Meta': {'object_name': 'DepartmentType', 'db_table': "'trainman_department_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trainman.teacher': {
            'Meta': {'ordering': "('user_profile__user__last_name',)", 'object_name': 'Teacher'},
            'degree': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherDegree']", 'db_column': "'id_degree'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherPosition']", 'db_column': "'id_position'"}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'user_profile'"})
        },
        'trainman.teacherdegree': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherDegree', 'db_table': "'trainman_teacher_degree'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.teacherposition': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherPosition', 'db_table': "'trainman_teacher_position'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.userprofile': {
            'Meta': {'ordering': "('user__last_name',)", 'object_name': 'UserProfile', 'db_table': "'trainman_user_profile'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pesel': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True', 'db_column': "'pesel'", 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'db_column': "'second_name'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'db_column': "'user'"})
        }
    }

    complete_apps = ['pythia']