# -*- coding: utf-8 -*-

import datetime
import math
import sets
from string import replace

from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.http import HttpResponseNotFound
from django.template import RequestContext
from django.template.response import TemplateResponse
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q, F
from django.db.models.aggregates import Sum, Count

from apps.merovingian.models import Subject, Course, semesters_list

from apps.trainman.models import Teacher
from apps.trainman.models import Department, UserOccupation

from apps.megacity.models import Student

from apps.pythia.forms import TokenLoginForm, course_students_number_formset_factory, \
    SearchForm, CourseSelectForm
from apps.pythia.forms import TeacherForm
from apps.pythia.forms import SubjectsForm
from apps.pythia.forms import question_answer_formset_factory
from apps.pythia.forms import question_text_formset_factory

from apps.pythia.models import Token, CourseProperties, Poll, \
    QUESTION_TYPE_OPEN_FINAL, QUESTION_TYPE_OPEN_EVALUATION, \
    QUESTION_TYPE_CLOSEDEND_EVALUATION, QUESTION_TYPE_CLOSEDEND_FINAL, \
    QUESTION_TYPE_GROUP, QUESTION_TYPE_GROUP_DESCRIPTION
    
from apps.pythia.models import RESULT_ALLOWED_LIST
from apps.pythia.models import Question
from apps.pythia.models import Answer
from apps.pythia.models import TokenAnswer
from apps.pythia.models import TokenText
from apps.pythia.models import ResultTeacher, ResultTeacherSubjectCourse
from apps.pythia.functions import make_page
from apps.pythia.printengine import page_pdf 
from apps.syjon.lib.functions import utf2ascii
from apps.whiterabbit.views import get_pdf_response



class Empty(object): pass

@login_required
@permission_required('pythia.change_courseproperties')
def index(request, template_name='pythia/index.html', current_app=None):
    """
    """
    m_qs = Course.objects.didactic_offer()
    all_students_cnt = CourseProperties.objects.filter(course__in=m_qs).aggregate(Sum('token_number'))
    all_students_cnt = all_students_cnt['token_number__sum'] if all_students_cnt['token_number__sum'] else 0

    courses_letters = []
    for m in m_qs.order_by('name').all():
        if m.name[0:1] not in courses_letters:
            courses_letters.append(m.name[0:1])

    context = {'all_students_cnt': all_students_cnt,
               'courses_letters': courses_letters}
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)

@login_required
@permission_required('pythia.change_courseproperties')
def stats(request, template_name='pythia/stats.html', current_app=None):
    
    course_id = None
    if 'course' in request.GET:
        course_id = request.GET['course']
    
    try:
        poll = Poll.objects.get(is_active=True)
    except Poll.DoesNotExist:
        poll = None
        
    context = {'poll': poll}
        
    if poll:
        
        open_questions = Question.objects.filter(Q(poll__exact=poll) & 
                                                 (Q(type__id__exact=QUESTION_TYPE_OPEN_FINAL) 
                                                   | Q(type__id__exact=QUESTION_TYPE_OPEN_EVALUATION)))

        # Prepare quesry set for tokens from this poll
        tokens_qs = Token.objects.filter(poll__exact=poll)
        
        # Get semesters list which are taken into account in this poll
        semesters = tokens_qs.values('semester')
        
        # Get usage for all courses
        tgroupped = tokens_qs.values('is_active').annotate(tcount=Count('is_active'))
        used_t = 0
        all_t = 0
        for g in tgroupped:
            if g['is_active']:
                all_t = g['tcount']
            else:
                used_t = g['tcount']
        all_t += used_t
            
        percent_t = int(math.floor(used_t * 100.0 / all_t)) if all_t > 0 else 0 
        
        # Get all students count
        all_s = CourseProperties.objects.filter(course_id__in=tokens_qs.values('course_id'), semester__in=semesters)\
                                .aggregate(scount=Sum('token_number'))['scount']
        percent_s = int(math.floor(used_t * 100.0 / all_s)) if all_s else 0
        
        # Get usage in course groupped by semesters
        semesters_usage = []
        sgroupped_all = Token.objects.filter(poll__exact=poll)\
                                .values('semester').annotate(tcount=Count('semester')).order_by('semester')
        sgroupped_used = Token.objects.filter(poll__exact=poll, is_active=False)\
                                .values('semester').annotate(tcount=Count('semester')).order_by('semester')
        sgroupped_students = CourseProperties.objects.filter(course_id__in=tokens_qs.values('course_id'))\
                                        .values('semester').annotate(scount=Sum('token_number'))
        for s in sgroupped_all:
            semesters_usage.append({'semester': s['semester'], 'all': s['tcount'], 'used' : 0, 'all_students': 0})
        for s in sgroupped_used:
            for su in semesters_usage:
                if su['semester'] == s['semester']:
                    su['used'] = s['tcount']
        for s in sgroupped_students:
            for su in semesters_usage:
                if su['semester'] == s['semester']:
                    su['all_students'] = s['scount']


        # Prepare course search form         
        courses_sq = tokens_qs.values('course_id')
        form = CourseSelectForm()
        form.set_courses(Course.objects.filter(id__in=courses_sq))
        
        # If course has been selected
        # Get usage for selected courses
        current_course = None
        mused_t = 0
        mall_t = 0
        mall_s = 0
        mpercent_t = 0
        mpercent_s = 0
        msemesters_usage = []
        if course_id:
            current_course = Course.objects.get(pk=course_id)
            
            # Get all and used tokens count
            tgroupped = Token.objects.filter(poll__exact=poll, course_id__exact=course_id)\
                            .values('is_active').annotate(tcount=Count('is_active'))
            for g in tgroupped:
                if g['is_active']:
                    mall_t = g['tcount']
                else:
                    mused_t = g['tcount']
            mall_t += mused_t
            mpercent_t = int(math.floor(mused_t * 100.0 / mall_t)) if mall_t > 0 else 0
            
            # Get all students count
            mall_s = CourseProperties.objects.filter(course_id__exact=course_id, semester__in=semesters)\
                                .aggregate(scount=Sum('token_number'))['scount']
            mpercent_s = int(math.floor(mused_t * 100.0 / mall_s))  if mall_s > 0 else 0
            
            # Get usage in course groupped by semesters
            courses_ids = [ m.id for m in current_course.get_courses() ]
            courses_ids.append(current_course.id)

            sgroupped_all = Token.objects.filter(poll__exact=poll, course__id__in=courses_ids)\
                                .values('semester').annotate(tcount=Count('semester')).order_by('semester')
            sgroupped_used = Token.objects.filter(poll__exact=poll, course__id__in=courses_ids, is_active=False)\
                                .values('semester').annotate(tcount=Count('semester')).order_by('semester')
            sgroupped_students = CourseProperties.objects.filter(course_id__in=courses_ids)\
                                .values('semester').annotate(scount=Sum('token_number'))

            for s in sgroupped_all:
                msemesters_usage.append({'semester': s['semester'], 'all': s['tcount'], 'used' : 0})
            for s in sgroupped_used:
                for su in msemesters_usage:
                    if su['semester'] == s['semester']:
                        su['used'] = s['tcount']
            for s in sgroupped_students:
                for su in msemesters_usage:
                    if su['semester'] == s['semester']:
                        su['all_students'] = s['scount']
            
        context.update({'used_t': used_t, 'all_t': all_t, 'percent_t': percent_t, 'all_s': all_s, 'percent_s': percent_s,
                'form': form, 'current_course': current_course,
               'mused_t': mused_t, 'mall_t': mall_t, 'mpercent_t': mpercent_t, 'mall_s': mall_s, 'mpercent_s': mpercent_s,
               'semesters_usage': semesters_usage, 'msemesters_usage': msemesters_usage,
               'open_questions': open_questions})
        
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)

@login_required
@permission_required('pythia.change_courseproperties')
def stats_question(request, question_id, template_name='pythia/stats_question.html', current_app=None):
    
    questions = Question.objects.filter(poll__is_active=True, id__exact=question_id)
    if len(questions) == 0:
        return HttpResponseNotFound()
    
    question = questions[0]
    answers_page = make_page(request, TokenText.objects.filter(question__id=question.id).exclude(text=''), 'pythia_questions')
    
    context = {'answers_page': answers_page}
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)

@login_required
@permission_required('pythia.change_courseproperties')
def count(request, letter, template_name='pythia/count.html', current_app=None):
    """
    """
    
    courses = Course.objects.didactic_offer().filter(name__startswith=letter).all()
    semesters = semesters_list()
    
    courses_students_number_formset = course_students_number_formset_factory(request.POST or None, request.FILES or None,
                                                              prefix='courses',
                                                              courses=courses)
    
    if request.method == "POST":
        if courses_students_number_formset.is_valid():
            for form in courses_students_number_formset.forms:
                form.save()
            messages.success(request, _(u'New numbers of students have been saved'))
        else:
            messages.error(request, _(u'There are errors in form. Correct fields marked with red color.'))
    
    context = {'courses_students_number_formset': courses_students_number_formset,
               'semesters': semesters, }
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)



def login(request):
    
    try:
        poll = Poll.objects.get(is_active=True)
    except Poll.DoesNotExist:
        poll = None
    
    token_form = TokenLoginForm(request.POST or None)
    if request.method == 'POST':
        if token_form.is_valid():
            try:
                token = Token.objects.get(name__exact=token_form.cleaned_data['token'],
                                          student__email__exact=token_form.cleaned_data['email'],
                                          is_active=True,
                                          poll__is_active=True)
                if not (token.poll.start_date <= datetime.date.today() <= token.poll.end_date):
                    raise Token.DoesNotExist

                subjects = Subject.objects.active().filter(semester__exact=token.semester,
                                                           module__sgroup__course__exact=token.course)
                if subjects.count() == 0:
                    raise Subject.DoesNotExist
                request.session['token_id'] = token.id
                request.session['subjects'] = subjects
                return redirect('apps.pythia.views.first')
            except Token.DoesNotExist:
                messages.error(request, _(u'Given token does not exist or is inactive.'))
                return redirect('apps.pythia.views.login')
            except Subject.DoesNotExist:
                messages.error(request, _(u'This token is assigned to a course without subjects, please try again later.'))
                return redirect('apps.pythia.views.login')
        else:
            messages.error(request, _(u'Either token or e-mail is invalid.'))

    kwargs = {'poll': poll, 'token_form': token_form}
    return render_to_response('pythia/login.html',
                              kwargs,
                              context_instance=RequestContext(request))


def login_link(request, email='', token=''):

    try:
        token = Token.objects.get(name__exact=token,
                                  student__email__exact=email,
                                  is_active=True,
                                  poll__is_active=True)
        if not (token.poll.start_date <= datetime.date.today() <= token.poll.end_date):
            raise Token.DoesNotExist

        subjects = Subject.objects.active().filter(semester__exact=token.semester,
                                                   module__sgroup__course__exact=token.course)
        if subjects.count() == 0:
            raise Subject.DoesNotExist
        request.session['token_id'] = token.id
        request.session['subjects'] = subjects
        return redirect('apps.pythia.views.first')
    except Token.DoesNotExist:
        messages.error(request, _(u'Given token does not exist or is inactive.'))
        return redirect('apps.pythia.views.login')
    except Subject.DoesNotExist:
        messages.error(request, _(u'This token is assigned to a course without subjects, please try again later.'))
        return redirect('apps.pythia.views.login')


def first(request):
    poll = Poll.objects.get(is_active=True)
    subjects = request.session.get('subjects', [])
    subjects_form = SubjectsForm(request.POST or None)
    subjects_form.set_subjects(subjects)
    if request.method == 'POST' and subjects_form.is_valid():
        request.session['pos'] = 0
        request.session['edata'] = [
            {
                'teacher': None,
                'subject': s,
                'question_answer': None,
                'question_text': None
            } for s in subjects_form.cleaned_data['subjects']
            ]
        request.session['ldata'] = [
            {
                'question_answer': None,
                'question_text': None
            }]
        return redirect('apps.pythia.views.wizard')
    if request.method == 'POST' and not subjects_form.is_valid():
        messages.error(request, _(u'Please select at least one subject.'))
    kwargs = {'poll': poll, 'subjects_form': subjects_form}
    return render_to_response('pythia/first.html',
                              kwargs,
                              context_instance=RequestContext(request))

# --- Teachers ---

def find_teacher(request):
    teachers = Teacher.objects.all()
    if request.method == 'POST':
        search_form = SearchForm(request.POST)
        if search_form.is_valid():
            name = search_form.cleaned_data['name']
            teachers = teachers.filter(user_profile__user__last_name__istartswith=name)
    else:
        search_form = SearchForm()

    kwargs = {'teachers_page': make_page(request, teachers),
              'search_form': search_form}
    return render_to_response('pythia/teachers/teacher_select.html',
                              kwargs,
                              context_instance=RequestContext(request))
    
def wizard(request):
    pos = request.session['pos']
    edata = request.session['edata'][pos]
    edata_len = len(request.session['edata'])

    teacher_form = TeacherForm(request.POST or {'teacher': edata['teacher'], 'subject': edata['subject']} or None)
    teacher_form.set_teachers(Subject.objects.get(id__exact=edata['subject']).teachers.all())
    teacher_form.set_subject(edata['subject'])
    
    try:
        if 'teacher' in edata and edata['teacher']: 
            t = Teacher.objects.get(pk=edata['teacher'])
            teacher_form.add_teacher(t)
    except Teacher.DoesNotExist:
        pass
    
    
    evaluation_closed_question = Question.objects.filter(type__id__exact=QUESTION_TYPE_CLOSEDEND_EVALUATION, poll__is_active=True)
    evaluation_opened_question = Question.objects.filter(type__id__exact=QUESTION_TYPE_OPEN_EVALUATION, poll__is_active=True)
    question_answer_formset = question_answer_formset_factory(request.POST or None, request.FILES or None,
                                                              prefix='aclosed',
                                                              initial=edata['question_answer'],
                                                              questions=evaluation_closed_question)
    question_text_formset = question_text_formset_factory(request.POST or None, request.FILES or None,
                                                          prefix='aopened',
                                                          initial=edata['question_text'],
                                                          questions=evaluation_opened_question)
    
    all_questions = Question.objects.filter(poll__is_active=True)
    
    if request.method == 'POST':
        
        if request.POST['selected_teacher']:
            try:
                t = Teacher.objects.get(pk=request.POST['selected_teacher'])
                teacher_form.add_teacher(t)
            except Teacher.DoesNotExist:
                pass
        
        if teacher_form.is_valid() and question_answer_formset.is_valid() and question_text_formset.is_valid():
            edata = {
                'teacher': teacher_form.cleaned_data['teacher'],
                'subject': teacher_form.cleaned_data['subject'],
                'question_answer': question_answer_formset.cleaned_data,
                'question_text': question_text_formset.cleaned_data
                }
            if pos < edata_len:
                request.session['edata'][pos] = edata
            else:
                request.session['edata'].append(edata)
    
            if request.POST.get('action') == 'next':
                pos += 1
            elif request.POST.get('action') == 'curr':
                pos += 1
                request.session['edata'].insert(pos, {
                        'teacher': None,
                        'subject': teacher_form.cleaned_data['subject'],
                        'question_answer': None,
                        'question_text': None
                        })
                edata_len = len(request.session['edata'])
            elif request.POST.get('action') == 'prev':
                pos -= 1
    
            request.session.modified = True
            if 0 <= pos < edata_len:
                request.session['pos'] = pos
                return redirect('apps.pythia.views.wizard')
            elif pos < 0:
                request.session['pos'] = 0
                return redirect('apps.pythia.views.first')
            elif edata_len <= pos:
                request.session['pos'] = edata_len - 1
                return redirect('apps.pythia.views.last')
        else:
            messages.error(request, 'Proszę poprawić błędy.')
            
    kwargs = {'pos': pos,
              'len': edata_len,
              'subject': Subject.objects.get(id__exact=edata['subject']),
              'teacher_form': teacher_form,
              'question_formsets': [question_answer_formset, question_text_formset],
              'all_questions': all_questions,
              'type_group_id': QUESTION_TYPE_GROUP,
              'type_group_description_id': QUESTION_TYPE_GROUP_DESCRIPTION, }
    return render_to_response('pythia/wizard.html',
                              kwargs,
                              context_instance=RequestContext(request))

def last(request):
    ldata = request.session['ldata'][0]
    last_closed_question = Question.objects.filter(type__id__exact=QUESTION_TYPE_CLOSEDEND_FINAL, poll__is_active=True)
    last_opened_question = Question.objects.filter(type__id__exact=QUESTION_TYPE_OPEN_FINAL, poll__is_active=True)
    question_answer_formset = question_answer_formset_factory(request.POST or None,
                                                              prefix='lclosed',
                                                              initial=ldata['question_answer'],
                                                              questions=last_closed_question)
    question_text_formset = question_text_formset_factory(request.POST or None,
                                                          prefix='lopened',
                                                          initial=ldata['question_text'],
                                                          questions=last_opened_question)
    if (request.method == 'POST'
        and question_answer_formset.is_valid()
        and question_text_formset.is_valid()):
        request.session['ldata'][0] = {'question_answer': question_answer_formset.cleaned_data,
                                       'question_text': question_text_formset.cleaned_data}
        request.session.modified = True
        if request.POST.get('action') == 'next':
            return redirect('apps.pythia.views.logout')
        elif request.POST.get('action') == 'prev':
            return redirect('apps.pythia.views.wizard')
        else:
            return redirect('apps.pythia.views.login')
    elif (request.method == 'POST'
          and not question_answer_formset.is_valid()
          and not question_text_formset.is_valid()):
        messages.error(request, _(u'Please correct errors.'))
    kwargs = {'question_formset': [question_answer_formset, question_text_formset]}
    return render_to_response('pythia/last.html',
                              kwargs,
                              context_instance=RequestContext(request))

def logout(request):
    edata = request.session.get('edata', [])
    ldata = request.session.get('ldata', [])
    try:
        token = Token.objects.get(id__exact=request.session['token_id'])
        for e in edata:
            subject = Subject.objects.get(id__exact=e['subject'])
            teacher = Teacher.objects.get(id__exact=e['teacher'])
            for a in e['question_answer']:
                TokenAnswer.objects.create(token=token,
                                           question=Question.objects.get(id__exact=a['question']),
                                           answer=Answer.objects.get(id__exact=a['answer']),
                                           subject=subject,
                                           teacher=teacher)
            for t in e['question_text']:
                if 'question' in t:
                    TokenText.objects.create(token=token,
                                             question=Question.objects.get(id__exact=t['question']),
                                             text=t['answer'],
                                             subject=subject,
                                             teacher=teacher)
        for l in ldata:
            for a in l['question_answer']:
                TokenAnswer.objects.create(token=token,
                                           question=Question.objects.get(id__exact=a['question']),
                                           answer=Answer.objects.get(id__exact=a['answer']),
                                           subject=None,
                                           teacher=None)
            for t in l['question_text']:
                if 'question' in t:
                    TokenText.objects.create(token=token,
                                             question=Question.objects.get(id__exact=t['question']),
                                             text=t['answer'],
                                             subject=None,
                                             teacher=None)
        token.is_active = False
        token.date = datetime.datetime.now()
        token.save()
    except:
        pass  # messages.error(request, 'Błąd.')
    kwargs = {'edata': request.session.get('edata', []),
              'ldata': request.session.get('ldata', [])}
    return render_to_response('pythia/logout.html',
                              kwargs,
                              context_instance=RequestContext(request))
    
##############################################
#                RESULTS
##############################################

@login_required
def result_polls(request):
    
    if not request.user.get_profile().teacher:
        return HttpResponseNotFound()
    
    polls_page = make_page(request, Poll.objects.filter(end_date__lt=datetime.datetime.now()), 'pythia_result_polls')
    
    kwargs = {'polls_page': polls_page}
    return render_to_response('pythia/result_polls.html',
                              kwargs,
                              context_instance=RequestContext(request))

@login_required
def result_poll_teacherlist(request, poll_id):
    if not request.user.get_profile().teacher:
        return HttpResponseNotFound()
    
    try:
        poll = Poll.objects.get(pk=poll_id)
    except Poll.DoesNotExist:
        return HttpResponseNotFound() 
      
    teachers = []
    departments = []
    
    for occupation in UserOccupation.objects.filter(user_profile=request.user.get_profile()):
        if occupation.occupation.id in RESULT_ALLOWED_LIST:
            departments+=Department.objects.filter(id__in=occupation.department.children_id())
            teachers+=Teacher.objects.filter(user_profile__department_id__in=occupation.department.children())
    
    teachers=sorted(list(set(teachers)), key=lambda item: item.user_profile.user.last_name)
    departments=list(set(departments))
    
    kwargs = {'own': request.user.get_profile().teacher, 'teachers': teachers, 'departments': departments,  'poll_id': poll_id, 'poll': poll}
    return render_to_response('pythia/result_teacherlist.html',
                              kwargs,
                              context_instance=RequestContext(request))
        
def user_allowed_to_see_teacher(viewer, viewed):
    for occupation in UserOccupation.objects.filter(user_profile__exact=viewer, occupation_id__in=RESULT_ALLOWED_LIST):
        if viewed.user_profile.department in occupation.department.children():
            return True
    return False
    
def user_allowed_to_see_department(viewer, viewed):
    for occupation in UserOccupation.objects.filter(user_profile__exact=viewer, occupation_id__in=RESULT_ALLOWED_LIST):
        if viewed in occupation.department.children():
            return True
    return False
        
@login_required
def result_poll(request, poll_id, teacher_id, subject_id=0):
    if not request.user.get_profile().teacher:
        return HttpResponseNotFound()  
    try:
        poll = Poll.objects.get(pk=poll_id)
    except Poll.DoesNotExist:
        return HttpResponseNotFound() 
    
    teacher = Teacher.objects.filter(id__exact=teacher_id)[0] 
    if (request.user.get_profile().teacher.id != int(teacher_id) \
        and not user_allowed_to_see_teacher(request.user.get_profile(), teacher)) \
        and not request.user.is_superuser :
        return HttpResponseNotFound() 
    
    if poll.end_date.month < 6:
        current_year = poll.end_date.year - 1
    else:
        current_year = poll.end_date.year
        
    subject = None
    if subject_id != 0:
        subject = Subject.objects.filter(id__exact=subject_id)[0]
    
    subject_results=ResultTeacherSubjectCourse.objects.filter(teacher_id__exact=teacher.id, poll_id__exact=poll_id)
    for sresults in subject_results:
        if sresults.mark==0:
            sresults.mark_str=""
        else:
            sresults.mark_str='%1.2f' % sresults.mark;
    
    
    results=ResultTeacher.objects.filter(teacher_id__exact=teacher.id, poll_id__exact=poll_id)[0]
    if results.mark==0:
        results.mark_str=""
    else:
        results.mark_str='%1.2f' % results.mark;

    if subject_id == 0:
        answers = TokenAnswer.objects.filter(teacher_id__exact=teacher_id)
        text_answers = TokenText.objects.filter(teacher_id__exact=teacher_id)
        detailed_results = result_table(request, poll_id, answers, text_answers, True)
    else:
        answers = TokenAnswer.objects.filter(teacher_id__exact=teacher_id, subject_id__exact=subject_id)
        text_answers = TokenText.objects.filter(teacher_id__exact=teacher_id, subject_id__exact=subject_id)
        detailed_results = result_table(request, poll_id, answers, text_answers, True)

    
    kwargs = {'poll': poll, 'subjects': subject_results, 'teacher': teacher, 'results': results, 'detailedresults': detailed_results, 'subject': subject, 'is_superuser': request.user.is_superuser}
    return render_to_response('pythia/result_poll.html',
                              kwargs,
                              context_instance=RequestContext(request))
    
@login_required
def result_poll_print(request, poll_id, teacher_id):
    if not request.user.get_profile().teacher:
        return HttpResponseNotFound()  
    try:
        poll = Poll.objects.get(pk=poll_id)
    except Poll.DoesNotExist:
        return HttpResponseNotFound() 
    
    teacher = Teacher.objects.filter(id__exact=teacher_id)[0] 
    if (request.user.get_profile().teacher.id != int(teacher_id) \
        and not user_allowed_to_see_teacher(request.user.get_profile(), teacher)) \
        and not request.user.is_superuser :
        return HttpResponseNotFound() 
    
    if poll.end_date.month < 6:
        current_year = poll.end_date.year - 1
    else:
        current_year = poll.end_date.year
        
   
    subject_results=ResultTeacherSubjectCourse.objects.filter(teacher_id__exact=teacher.id, poll__exact=poll)
    for sresults in subject_results:
        if sresults.mark==0:
            sresults.mark_str=""
        else:
            sresults.mark_str='%1.2f' % sresults.mark;
    
    
    results=ResultTeacher.objects.filter(teacher_id__exact=teacher.id, poll__exact=poll)[0]
    if results.mark==0:
        results.mark_str=""
    else:
        results.mark_str='%1.2f' % results.mark;

    
    answers = TokenAnswer.objects.filter(teacher_id__exact=teacher_id)
    text_answers = TokenText.objects.filter(teacher_id__exact=teacher_id)
    detailed_results = result_table(request, poll_id, answers, text_answers, True)
    
    subject_detailed_results=[]
    for s in subject_results:
        answers = TokenAnswer.objects.filter(teacher_id__exact=teacher_id, subject_id__exact=s.subject.id)
        text_answers = TokenText.objects.filter(teacher_id__exact=teacher_id, subject_id__exact=s.subject.id)
        current_detailed_results = result_table(request, poll_id, answers, text_answers, True)
        current_detailed_results.subject=s.subject
        current_detailed_results.course=s.course
        subject_detailed_results.append(current_detailed_results)

    
    kwargs = {'poll': poll, 'subjects': subject_results, 'teacher': teacher, 'results': results, 'detailedresults': detailed_results, 'subjectdetailedresults': subject_detailed_results}
    page=render_to_string('pythia/result_poll_print.html', kwargs, context_instance=RequestContext(request))
    #return page_pdf(page, utf2ascii(replace(teacher.get_full_name(), ' ', '_')), request)
    return get_pdf_response(request, kwargs, 'pythia/result_poll_print.html', utf2ascii(replace(teacher.get_full_name(), ' ', '_')), reverse('apps.pythia.views.result_poll', kwargs={'poll_id': poll_id, 'teacher_id': teacher_id}),{'-O': 'landscape'})
    
@login_required    
def result_poll_department(request, poll_id, department_id, mode="details"):
    if not request.user.get_profile().teacher:
        return HttpResponseNotFound()  
    try:
        poll = Poll.objects.get(pk=poll_id)
    except Poll.DoesNotExist:
        return HttpResponseNotFound() 
    
    department = Department.objects.filter(id__exact=department_id)[0] 
    if not user_allowed_to_see_department(request.user.get_profile(), department) \
        and not request.user.is_superuser :
        return HttpResponseNotFound() 
    
    if poll.end_date.month < 6:
        current_year = poll.end_date.year - 1
    else:
        current_year = poll.end_date.year
    
    mark=0.0
    marked=0
    votes=0
    
    department_answers=ResultTeacher.objects.filter(teacher__user_profile__department__in=department.children(), poll__exact=poll)
    for answer in department_answers:
        if answer.votes > 0:
            mark+=answer.mark
            marked+=1
            votes+=answer.votes
    if marked > 0:
        mark/=marked
    if mark==0:
       mark_str=""
    else:
       mark_str='%1.2f' % mark;
    
    results = (mark_str, votes)
    ranking = []
    detailed_results = []
    if(mode == "detailed"): 
        answers = TokenAnswer.objects.filter(teacher__user_profile__department_id__in=department.children(), question__poll__exact=poll)
        text_answers = TokenText.objects.filter(teacher__user_profile__department_id__in=department.children(), question__poll__exact=poll)
        detailed_results = result_table(request, poll_id, answers, text_answers, True)
    if(mode == "personal"):   
        teachers = ResultTeacher.objects.filter(teacher__user_profile__department_id__in=department.children(), poll__exact=poll)    
        for teacher in teachers:
            if teacher.mark == 0:
                teacher.mark_str = ''
            else:
                teacher.mark_str = '%1.2f' % teacher.mark
            ranking.append((teacher.teacher, teacher.votes, teacher.mark, teacher.mark_str))
        ranking = sorted(sorted(ranking, key=lambda x:x[1], reverse=True), key=lambda x:x[2], reverse=True)
    elif(mode == "department"):
        departments = Department.objects.filter(department_id__exact=department.id)
        for current_department in departments:
            teachers = ResultTeacher.objects.filter(teacher__user_profile__department_id__in=current_department.children(), poll__exact=poll)
            mark=0.0
            marked=0
            votes=0    
            for teacher in teachers:
                if teacher.votes > 0:
                    marked+=1
                    votes+=teacher.votes
                    mark+=teacher.mark
            if mark == 0:
                mark_str = ''
            else:
                mark/=marked
                mark_str = '%1.2f' % mark
                
            #department_answers = TokenAnswer.objects.filter(teacher__user_profile__department_id__in=current_department.children())
            #department_results = result_table(request, poll_id, department_answers, _, False)
            ranking.append((current_department, votes, marked, mark, mark_str))
        ranking = sorted(sorted(ranking, key=lambda x:x[1], reverse=True), key=lambda x:x[3], reverse=True)
        
    counter = 0
    numbered_ranking = []
    for item in ranking:
        counter += 1
        numbered_ranking.append(item + (counter,))
        

    
    
    kwargs = {'poll': poll, 'department': department, 'detailed_results': detailed_results, 'ranking':numbered_ranking, 'mode': mode, 'results': results, 'is_superuser': request.user.is_superuser}
    return render_to_response('pythia/result_department.html',
                 kwargs,
                 context_instance=RequestContext(request))
    
    
def result_table(request, poll_id, results, text_results, detailed=False):
    questions = Question.objects.filter(poll_id__exact=poll_id).order_by('sequence')
    result = Empty()
    result.parts = []
    result.votes = len(results.filter(question__type__exact=QUESTION_TYPE_CLOSEDEND_EVALUATION)) / len(Question.objects.filter(type__exact=QUESTION_TYPE_CLOSEDEND_EVALUATION, poll__exact=poll_id))
    result.mark = 0.0
    weight_sum = 0.0
    result.weight = Question.objects.filter(poll_id__exact=poll_id, type=QUESTION_TYPE_CLOSEDEND_EVALUATION).aggregate(Sum('weight'))['weight__sum']

    
    for question in questions:
        if detailed and question.type.id == QUESTION_TYPE_GROUP:
            part = Empty()
            part.type = 1
            part.content = question.name
            result.parts.append(part)
        elif detailed and question.type.id == QUESTION_TYPE_GROUP_DESCRIPTION:
            part = Empty()
            part.type = 2
            part.content = question.name
            result.parts.append(part)
        elif question.type.id == QUESTION_TYPE_CLOSEDEND_EVALUATION:
            if detailed:

                answers = []
                for answer in Answer.objects.filter(question_id__exact=question.id).order_by('sequence'):
                    answers.append(answer.name)
                
                part = result.parts[-1]

                if(part.type == QUESTION_TYPE_CLOSEDEND_EVALUATION and set(part.answers) == set(answers)):
                    part.questions.append(question)
                else:
                    part = Empty()
                    part.type = 3
                    part.questions = [question]
                    part.answers = answers
                    result.parts.append(part)
                part.questions[-1].answers = {}
                for answer in answers:
                    part.questions[-1].answers[answer] = 0
                part.questions[-1].weight_str = "%1.0f" % (float(question.weight) * 100. / float(result.weight))


            question_answers = results.filter(question_id__exact=question.id)
            if detailed:
                for answer in answers:    
                    part.questions[-1].answers[answer] = len(question_answers.filter(answer__name__exact=answer))

            mark = 0.0; weight = 0.0
            for answer in question_answers:
                mark += float(answer.answer.mark) * float(answer.answer.weight)
                weight += float(answer.answer.weight)
            weight_sum += float(weight) * float(question.weight);
            if detailed and weight == 0:
                part.questions[-1].mark = 0
            else:
                result.mark += mark * float(question.weight)
                if weight == 0:
                    mark = 0
                else:
                    mark = float(mark / weight)
                if detailed:
                    part.questions[-1].mark_str = '%1.2f' % mark
            
                
        elif detailed and  question.type.id == QUESTION_TYPE_OPEN_EVALUATION:
            part = Empty()
            part.type = 4
            part.content = question.name
            part.answers = []
            for answer in text_results.filter(question_id__exact=question.id):
                if(len(answer.text.strip()) > 2):
                    if int(poll_id) >= 3:
                        student=Student.objects.filter(id__exact=answer.token.student.id)[0]
                        answer.student=student.first_name+' '+student.last_name
                    part.answers.append(answer)
            result.parts.append(part)
    
    if result.votes == 0 or weight_sum == 0:
        result.mark = 0
        result.mark_str = ''
    else:
        result.mark = result.mark / weight_sum
        result.mark_str = '%1.2f' % result.mark
        
    
    return result
    
