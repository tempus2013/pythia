# -*- coding: utf-8 -*-
'''
Created on 25-02-2013

@author: Damian Rusinek
'''
import unicodedata
import re

def item_per_page():
    return 25

def make_page(request, objects, objects_id = 'pythia'):
    from django.core.paginator import Paginator
    from django.core.paginator import InvalidPage
    from django.core.paginator import EmptyPage
    paginator = Paginator(objects, item_per_page())
    
    try:
        page = request.GET.get('page', None)
        if objects_id is not None:
            if abs(len(objects) - request.session.get(objects_id + '_objects', len(objects))) > 1:
                objects_page = paginator.page(1)
            elif page is not None:
                objects_page = paginator.page(int(page))
            else:
                objects_page = paginator.page(int(request.session.get(objects_id + '_page', '1')))
        else: 
            objects_page = paginator.page(int(page))
    except (TypeError, ValueError, EmptyPage, InvalidPage):
        objects_page = paginator.page(1)
    finally:
        if objects_id is not None:
            request.session[objects_id + '_page'] = objects_page.number
            request.session[objects_id + '_objects'] = len(objects)
        return objects_page
    

def slugify(s):
    slug = unicodedata.normalize('NFKD', s)
    slug = slug.replace(u'ł', u'l').encode('ascii', 'ignore').lower()
    slug = re.sub(r'[^a-z0-9]+', '-', slug).strip('-')
    slug = re.sub(r'[-]+', '-', slug)
    return slug