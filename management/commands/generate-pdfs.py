# -*- coding: utf-8 -*-
import hashlib
import random
import csv
import datetime
import math
import unicodedata
import re
from optparse import make_option
from os import listdir, pardir, makedirs, system, remove
from os.path import join, dirname, isfile, isdir

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import translation

from syjon.settings import STATIC_URL, STATIC_ROOT
from syjon import config
from apps.pythia.functions import slugify

class Command(BaseCommand):
    args = '<input dir> <output dir>'

    TOKEN_TEMPLATE_FILE = join(STATIC_ROOT, 'pythia', 'token', 'token-template.html')
    TOKEN_WRAPPER_FILE = join(STATIC_ROOT, 'pythia', 'token', 'token-wrapper.html')
    TEMP_FILE = 'token.html'

    def handle(self, *args, **options):
        translation.activate('pl')
        
        command_file = '%spythia/token/wkhtmltopdf-%s' % (STATIC_ROOT, config.MACHINE_ARCHITECTURE)
        if not isfile(command_file):
            raise CommandError("Plik wykonywalny " + command_file + " nie istnieje")
        
        if len(args) != 2:
            raise CommandError('Zła liczba parametrów. Wymagane są dwa parametry: ścieżka do katalogu ' \
                + 'tokenami wyexportowanymi do CSV i ścieżka do katalogu wynikowego.')
        
        if not isfile(self.TOKEN_TEMPLATE_FILE) or not isfile(self.TOKEN_WRAPPER_FILE):
            raise CommandError('Plik z szablonem tokena nie istnieje: ' + self.TOKEN_TEMPLATE_FILE + ', ' + self.TOKEN_WRAPPER_FILE)
        
        if not isdir(args[0]):
            raise CommandError('Podana ścieżka do katalogu z plikami CSV jest nieprawidłowa')
        
        if not isdir(args[1]):
            makedirs(args[1])
                
        f = open(self.TOKEN_WRAPPER_FILE, 'r')
        token_wrapper_html = f.read()
        f.close()
        
        f = open(self.TOKEN_TEMPLATE_FILE, 'r')
        token_template_html = f.read()
        f.close()
        
        for f in listdir(args[0]):
            if not isfile(args[0]+f):
                continue
            if f[-4:] != ".csv":
                continue
            
            file = open(args[0] + f, 'r')
            csv_file = csv.reader(file, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_ALL)
            
            rows = list(csv_file)
            
            tokens_html = ""

            i = 0
            while i < len(rows):
                row = rows[i]
                i += 1
                    
                tmp_token_template = token_template_html
                
                type_name = '';
                if row[3] == 'niestacjonarny':
                    type_name = ' tryb niestacjonarny,'
                
                tmp_token_template = tmp_token_template.replace('{course}', row[0])
                tmp_token_template = tmp_token_template.replace('{department}', row[1])
                tmp_token_template = tmp_token_template.replace('{level}', row[2])
                tmp_token_template = tmp_token_template.replace('{type}', type_name)
                tmp_token_template = tmp_token_template.replace('{semester}', row[4])
                tmp_token_template = tmp_token_template.replace('{token}', row[5])
                
                tokens_html += tmp_token_template 
                if i % 2 == 0:
                    tokens_html += '<div class="clear"></div><div class="break"></div>'
                    
            if tokens_html != "":
                
                tokens_html = token_wrapper_html\
                                    .replace('{STATIC_URL}', STATIC_URL)\
                                    .replace('{BODY}', tokens_html)
                    
                token_file = open(self.TEMP_FILE, 'w')
                token_file.write(tokens_html)
                token_file.flush()
                token_file.close()
                    
                pdf_filename = args[1]+f[0:-4]+'.pdf'
                
                command = '%s --page-size A4 --margin-top 0 --margin-bottom 0 --margin-left 0 --margin-right 0 %s %s' % (command_file, self.TEMP_FILE, pdf_filename)
                system(command)
            
            file.close()

        remove(self.TEMP_FILE)
            
            
        

