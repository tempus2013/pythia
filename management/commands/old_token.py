# -*- coding: utf-8 -*-

import hashlib
import random
import csv
import datetime
import math
from optparse import make_option

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import translation
from django.core.exceptions import ObjectDoesNotExist

from apps.merovingian.models import Course
from apps.pythia.models import Poll
from apps.pythia.models import CourseProperties
from apps.pythia.models import Token
from apps.trainman.models import Department
from apps.pythia.functions import slugify

class Command(BaseCommand):
    TOKEN_SIZE = 10
    ADDED_PERCENTAGE = 0.1
    UNI_NAME = "UMCS"
    W = [1, 3, 5, 7, 9]
    S = [2, 4, 6, 8, 10]
    args = '<w|s> <dir>'

    university_department = None

    def _add_token_row(self, csv_writer, token):
        """
        """
        
        department = token.course.department
        while department and department.department != self.university_department:
            department = department.department
            
        department_name = unicode(department.name).encode('utf-8') if department else ''
        
        name = unicode(token.course.name).encode('utf-8')
        type_name = unicode(token.course.type.name).encode('utf-8')
        level_name = unicode(token.course.level.name).encode('utf-8')
        
        csv_writer.writerow([name, department_name, level_name, type_name, token.semester, token.name])

    def handle(self, *args, **options):
        translation.activate('pl')
        if len(args) != 2:
            raise CommandError('Wrong number of parameters: %s' % self.args)
        
        try:
            self.university_department = Department.objects.get(name=self.UNI_NAME)
        except Department.DoesNotExist:
            raise CommandError('Cannot find the main department representing university. Has it changed?')
        
        all_sems = []
        if args[0] == 'w':
            all_sems = self.W
        elif args[0] == 's':
            all_sems = self.S
            
        for course in Course.objects.didactic_offer():
            file_name = args[1] + slugify(unicode(course)) + '.csv' 
            print file_name
            f = open(file_name, 'w')
            csv_file = csv.writer(f, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_ALL)
            
            sems = list(set(all_sems).intersection(set(course.get_current_semesters())))
            
            for s in sems:
                try:
                    mp = CourseProperties.objects.get(course__exact = course, semester__exact = s)
                    existing_tokens_cnt = Token.objects.filter(course__exact = course, semester__exact = s).count()
                    
                    if existing_tokens_cnt > 0:
                        for token in Token.objects.filter(course__exact = course, semester__exact = s).all():
                            self._add_token_row(csv_file, token)
                    
                    for i in range(existing_tokens_cnt, int(math.ceil( (1 + self.ADDED_PERCENTAGE) *  mp.token_number))):
                        token_str = self.get_token(course)
                        if Token.objects.filter(name__exact = token_str).exists():
                            continue
                        token = Token.objects.create(name = token_str,
                                             date = datetime.date.today(),
                                             semester = s,
                                             course = course,
                                             poll = Poll.objects.get(is_active = True))
                        self._add_token_row(csv_file, token)
                        
                except CourseProperties.DoesNotExist:
                    pass
                except Poll.DoesNotExist:
                    f.close()
                    raise CommandError('Brak aktywnej ankiety!')
            f.close()

    def get_token(self, course):
        hash_str = str(random.random()) + unicode(course).encode('ascii', 'replace') + str(random.random())
        return random.sample('abcdef', 1)[0] + ''.join(random.sample(hashlib.sha512(hash_str).hexdigest(), self.TOKEN_SIZE - 1)) 
