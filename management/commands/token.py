# -*- coding: utf-8 -*-

import hashlib
import random
import csv
import datetime
import math
from optparse import make_option

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import translation
from django.core.exceptions import ObjectDoesNotExist
from apps.megacity.models import Student

from apps.merovingian.models import Course
from apps.pythia.models import Poll
from apps.pythia.models import CourseProperties
from apps.pythia.models import Token
from apps.trainman.models import Department
from apps.pythia.functions import slugify

class Command(BaseCommand):
    TOKEN_SIZE = 10
    ADDED_PERCENTAGE = 0.1
    UNI_NAME = "UMCS"
    W_TYPE = 'w'
    S_TYPE = 's'
    W = [1, 3, 5, 7, 9]
    S = [2, 4, 6, 8, 10]
    args = '<w|s>'

    university_department = None

    def _add_token_row(self, csv_writer, token):
        """
        """
        
        department = token.course.department
        while department and department.department != self.university_department:
            department = department.department
            
        department_name = unicode(department.name).encode('utf-8') if department else ''
        
        name = unicode(token.course.name).encode('utf-8')
        type_name = unicode(token.course.type.name).encode('utf-8')
        level_name = unicode(token.course.level.name).encode('utf-8')
        
        csv_writer.writerow([name, department_name, level_name, type_name, token.semester, token.name])

    def handle(self, *args, **options):
        translation.activate('pl')
        if len(args) != 1:
            raise CommandError('Wrong number of parameters: %s' % self.args)
        
        try:
            self.university_department = Department.objects.get(name=self.UNI_NAME)
        except Department.DoesNotExist:
            raise CommandError('Cannot find the main department representing university. Has it changed?')

        try:
            active_poll = Poll.objects.get(is_active = True)
        except Poll.DoesNotExist:
            raise CommandError('No active poll.')

        active_courses = Course.objects.didactic_offer()
        for student in Student.objects.filter(courses__in=active_courses).select_related('courses'):
            for course in student.courses.filter(id__in=active_courses):
                semester = self.get_semester(course, args[0])
                try:
                    Token.objects.get(student=student, semester=semester, course=course, poll=active_poll)
                except Token.DoesNotExist:
                    Token.objects.create(name=self.get_token(course), semester=semester,
                                         course=course, poll=active_poll, student=student)

    def get_token(self, course):
        hash_str = str(random.random()) + unicode(course).encode('ascii', 'replace') + str(random.random())
        return random.sample('abcdef', 1)[0] + ''.join(random.sample(hashlib.sha512(hash_str).hexdigest(),
                                                                     self.TOKEN_SIZE-1))

    def get_semester(self, course, sem_type):
        course_year = course.start_date.year
        current_year = datetime.datetime.now().year
        if sem_type == self.S_TYPE:
            return self.S[current_year-course_year-1]
        else: # sem_type == self.W_TYPE
            return self.W[current_year-course_year-1]