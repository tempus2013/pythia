# -*- coding: utf-8 -*-
import smtplib

import unicodedata
import re
from django.core.mail.message import BadHeaderError

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import translation
from django.core.urlresolvers import reverse
from django.utils.html import strip_tags

from syjon.settings import STATIC_URL, STATIC_ROOT
from syjon import config
from apps.megacity.models import Student
from apps.pythia.models import Token

from django.core.files import File
from django.core.mail import get_connection, EmailMultiAlternatives
from django.core.mail.backends.smtp import EmailBackend
from email.MIMEImage import MIMEImage
from string import replace
from apps.pythia.settings import INVITATION_EMAIL_HOST, INVITATION_EMAIL_PORT, INVITATION_EMAIL_HOST_USER, INVITATION_EMAIL_HOST_PASSWORD, INVITATION_EMAIL_USE_TLS, INVITATION_DEFAULT_FROM_EMAIL


class Command(BaseCommand):
    args = u'<id_poll>'

    def handle(self, *args, **options):

        def text_content(body):
            regexp1 = re.compile(r'<\ *\\?\ *br\ *\\?\ *>', flags=re.IGNORECASE)
            return strip_tags(regexp1.sub("\n", body))

        current_poll_id = args[0]

        connection = get_connection(host=INVITATION_EMAIL_HOST,
                                    port=INVITATION_EMAIL_PORT,
                                    username=INVITATION_EMAIL_HOST_USER,
                                    password=INVITATION_EMAIL_HOST_PASSWORD,
                                    use_tls=INVITATION_EMAIL_USE_TLS)
        connection.open()
        
        imglist = ["kreska.jpg", "kwadraty_33px.jpg", "orzel_66_px.jpg", "UMCS_66_px.jpg"]
        imgmime = []
        
        for img in imglist:
            imgfile = open(STATIC_ROOT+"/pythia/invitation/"+img, 'rb')
            mime=MIMEImage(imgfile.read())
            mime.add_header('Content-ID','<%s>' % img)
            imgmime.append(mime)
            imgfile.close()
        
        content_file = open(STATIC_ROOT+"/pythia/invitation/invitation.html", 'r')
        mail_content = content_file.read()
        mail_content = mail_content.decode('utf-8')

        for token in Token.objects.filter(poll_id__exact=current_poll_id, sent=False):
            print token.student.email
            #link = "http://127.0.0.1:8000"+reverse("apps.pythia.views.login_link", kwargs={"email": token.student.email, "token": token.name})
            link = "http://syjon.umcs.lublin.pl"+reverse("apps.pythia.views.login_link", kwargs={"email": token.student.email, "token": token.name})
            personalized_content = replace(mail_content, 'poll_link', link)
            mail = EmailMultiAlternatives(
                subject="Ankieta Oceny Zajęć UMCS – aktywna od 8 czerwca!",
                body=text_content(personalized_content),
                from_email=INVITATION_DEFAULT_FROM_EMAIL,
                to=[token.student.email],
                connection=connection)
            mail.attach_alternative(personalized_content, "text/html")
            for mime in imgmime:
                mail.attach(mime)
            try:
                mail.send()
                token.sent = True
                token.save()
            except smtplib.SMTPRecipientsRefused:
                continue
            except smtplib.SMTPResponseException:
                continue
            except BadHeaderError:
                continue
        connection.close()
