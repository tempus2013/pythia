# -*- coding: utf-8 -*-

import codecs
import os
from string import replace

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import translation
from django.db.models.aggregates import Sum
from django.template.loader import render_to_string
from django.template import RequestContext
from django.http import HttpRequest, HttpResponseNotFound

from apps.trainman.models import Department
from apps.pythia.models import Question
from apps.pythia.models import Answer
from apps.pythia.models import ResultTeacher, ResultTeacherSubjectCourse
from apps.trainman.models import Teacher
from apps.pythia.models import TokenAnswer
from apps.pythia.models import TokenText

from apps.syjon.lib.functions import utf2ascii
from apps.pythia.models import CourseProperties, Poll, \
    QUESTION_TYPE_OPEN_EVALUATION, \
    QUESTION_TYPE_CLOSEDEND_EVALUATION, \
    QUESTION_TYPE_GROUP, QUESTION_TYPE_GROUP_DESCRIPTION



import syjon


class Empty(object): pass

class Command(BaseCommand):
    
    args = u'<id_poll> <id_department>'
    


    
    def result_table(self, poll_id, results, text_results, detailed=False):
        questions = Question.objects.filter(poll_id__exact=poll_id).order_by('sequence')
        result = Empty()
        result.parts = []
        result.votes = len(results.filter(question__type__exact=QUESTION_TYPE_CLOSEDEND_EVALUATION)) / len(Question.objects.filter(type__exact=QUESTION_TYPE_CLOSEDEND_EVALUATION, poll__exact=poll_id))
        result.mark = 0.0
        weight_sum = 0.0
        result.weight = Question.objects.filter(poll_id__exact=poll_id, type=QUESTION_TYPE_CLOSEDEND_EVALUATION).aggregate(Sum('weight'))['weight__sum']
    
        
        for question in questions:
            if detailed and question.type.id == QUESTION_TYPE_GROUP:
                part = Empty()
                part.type = 1
                part.content = question.name
                result.parts.append(part)
            elif detailed and question.type.id == QUESTION_TYPE_GROUP_DESCRIPTION:
                part = Empty()
                part.type = 2
                part.content = question.name
                result.parts.append(part)
            elif question.type.id == QUESTION_TYPE_CLOSEDEND_EVALUATION:
                if detailed:
    
                    answers = []
                    for answer in Answer.objects.filter(question_id__exact=question.id).order_by('sequence'):
                        answers.append(answer.name)
                    
                    part = result.parts[-1]
    
                    if(part.type == QUESTION_TYPE_CLOSEDEND_EVALUATION and set(part.answers) == set(answers)):
                        part.questions.append(question)
                    else:
                        part = Empty()
                        part.type = 3
                        part.questions = [question]
                        part.answers = answers
                        result.parts.append(part)
                    part.questions[-1].answers = {}
                    for answer in answers:
                        part.questions[-1].answers[answer] = 0
                    part.questions[-1].weight_str = "%1.0f" % (float(question.weight) * 100. / float(result.weight))
    
    
                question_answers = results.filter(question_id__exact=question.id)
                if detailed:
                    for answer in answers:    
                        part.questions[-1].answers[answer] = len(question_answers.filter(answer__name__exact=answer))
    
                mark = 0.0; weight = 0.0
                for answer in question_answers:
                    mark += float(answer.answer.mark) * float(answer.answer.weight)
                    weight += float(answer.answer.weight)
                weight_sum += float(weight) * float(question.weight);
                if detailed and weight == 0:
                    part.questions[-1].mark = 0
                else:
                    result.mark += mark * float(question.weight)
                    if weight == 0:
                        mark = 0
                    else:
                        mark = float(mark / weight)
                    if detailed:
                        part.questions[-1].mark_str = '%1.2f' % mark
                
                    
            elif detailed and  question.type.id == QUESTION_TYPE_OPEN_EVALUATION:
                part = Empty()
                part.type = 4
                part.content = question.name
                part.answers = []
                for answer in text_results.filter(question_id__exact=question.id):
                    if(len(answer.text.strip()) > 2):
                        part.answers.append(answer)
                result.parts.append(part)
        
        if result.votes == 0 or weight_sum == 0:
            result.mark = 0
            result.mark_str = ''
        else:
            result.mark = result.mark / weight_sum
            result.mark_str = '%1.2f' % result.mark
            
        
        return result
    
    def result_poll_print(self, poll_id, teacher_id, path):
        try:
            poll = Poll.objects.get(pk=poll_id)
        except Poll.DoesNotExist:
            return HttpResponseNotFound() 
        
        teacher = Teacher.objects.filter(id__exact=teacher_id)[0] 
           
        if poll.end_date.month < 6:
            current_year = poll.end_date.year - 1
        else:
            current_year = poll.end_date.year
            
       
        subject_results=ResultTeacherSubjectCourse.objects.filter(teacher_id__exact=teacher.id, poll_id=poll_id)
        for sresults in subject_results:
            if sresults.mark==0:
                sresults.mark_str=""
            else:
                sresults.mark_str='%1.2f' % sresults.mark;
        
       	try: 
	        results=ResultTeacher.objects.filter(teacher_id__exact=teacher.id, poll_id=poll_id)[0]
	except:
		return
        if results.mark==0:
            results.mark_str=""
        else:
            results.mark_str='%1.2f' % results.mark;
    
        
        answers = TokenAnswer.objects.filter(teacher_id__exact=teacher_id)
        text_answers = TokenText.objects.filter(teacher_id__exact=teacher_id)
        detailed_results = self.result_table(poll_id, answers, text_answers, True)
        
        subject_detailed_results=[]
        for s in subject_results:
            answers = TokenAnswer.objects.filter(teacher_id__exact=teacher_id, subject_id__exact=s.subject.id)
            text_answers = TokenText.objects.filter(teacher_id__exact=teacher_id, subject_id__exact=s.subject.id)
            current_detailed_results = self.result_table(poll_id, answers, text_answers, True)
            current_detailed_results.subject=s.subject
            current_detailed_results.course=s.course
            subject_detailed_results.append(current_detailed_results)
    
       	try: 
            request=HttpRequest();
            kwargs = {'poll': poll, 'subjects': subject_results, 'teacher': teacher, 'results': results, 'detailedresults': detailed_results, 'subjectdetailedresults': subject_detailed_results}
            page=render_to_string('pythia/result_poll_print.html', kwargs,context_instance=RequestContext(request))
        
            created_name = utf2ascii(replace(teacher.__unicode__(), ' ', '_'))+'_'+`teacher.id`
            filename_HTML = created_name+'.html'
            filename_PDF = created_name+'.pdf'
        

            output_file = codecs.open(filename_HTML, 'w','utf-8')
            output_file.write(page)
            output_file.close()
     
        except:
            return
       
        # Stworzenie pliku PDF
        command = 'xvfb-run wkhtmltopdf -O landscape %s %s' % (filename_HTML, filename_PDF)
        os.system(command)
        os.system('rm '+filename_HTML)
        os.system('mv '+filename_PDF+' '+replace(path,' ','\\ ')+'/')

    def level(self, id_poll, department, path):
        current_path=path+'/'+utf2ascii(department.__unicode__())
        if not os.path.exists(current_path):
             os.makedirs(current_path)
        print department
        teachers = Teacher.objects.filter(user_profile__department_id__exact=department.id)
        for teacher in teachers:
            print teacher
            self.result_poll_print(id_poll, teacher.id, current_path)
                
        departments=Department.objects.filter(department_id__exact=department.id)
        for d in departments:
            self.level(id_poll, d,current_path)
                    

    def handle(self, *args, **options):
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))
        
        id_poll = args[0]
        id_department = args[1]
        
        department=Department.objects.filter(id__exact=id_department)[0]
        
        self.level(id_poll, department,'./')
            
        #teachers = Teacher.objects.filter(user_profile__department_id__in=department.children())
               
        #for teacher in teachers:
        #   print teacher
        #   self.result_poll_print(id_poll, teacher.id)
        
        
           
            
            
        
        

