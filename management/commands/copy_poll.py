# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import translation
from apps.pythia.models import Poll, Question
from django.conf import settings

import syjon
from django.db import transaction


class Command(BaseCommand):
    args = '<poll_id>'
    help = 'Copies existing poll and creates new one with the same questions and answers'

    @transaction.commit_on_success
    def copy_poll(self, poll):
        
        old_pk = poll.pk
        
        poll.pk = None
        poll.is_active = False
        poll.name = "COPIED"
        for (lang_code, _) in settings.LANGUAGES:
            setattr(poll, 'name_'+lang_code, "COPIED")
        poll.save()
        new = poll
        old = Poll.objects.get(pk=old_pk)
        
        for question in old.question_set.all():
            old_question_pk = question.pk
            
            question.pk = None
            question.poll = new
            question.save()
            new_question = question
            
            old_question = Question.objects.get(pk=old_question_pk)
            
            for answer in old_question.answer_set.all():
                answer.pk = None
                answer.question = new_question
                answer.save()
            
        print 'Copied poll {0}'.format(old)
            
    def handle(self, *args, **options):
        
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))
        
        if len(args) != 1:
            raise CommandError('Wrong number of parameters. Expected: ' + self.args)
        
        poll_id = int(args[0])
        
        poll = Poll.objects.get(pk=poll_id)
        self.copy_poll(poll)
