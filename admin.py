# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.admin.options import ModelAdmin

from apps.pythia.models import Poll
from apps.pythia.models import CourseProperties
from apps.pythia.models import QuestionType
from apps.pythia.models import Question
from apps.pythia.models import Answer
from apps.pythia.models import Token
from apps.pythia.models import TokenAnswer
from apps.pythia.models import TokenText


class QuestionAdmin(ModelAdmin):
    list_filter = ('poll', 'type')
    
class AnswerAdmin(ModelAdmin):
    list_filter = ('question',)

admin.site.register(Poll)
admin.site.register(CourseProperties)
admin.site.register(QuestionType)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Token)
admin.site.register(TokenAnswer)
admin.site.register(TokenText)
